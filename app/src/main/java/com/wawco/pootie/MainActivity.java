package com.wawco.pootie;

import java.util.HashMap;
import java.util.concurrent.*;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.*;
import android.media.*;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends Activity implements OnClickListener {
        
    public static SoundPool soundPool;
    private AudioManager audioManager;
    private HashMap<Integer, Boolean> buttonClicks;
    private HashMap<Integer, Integer> mapSoundToButtons;
    private static MediaPlayer mp;
    
    // Variables relating to scheduling HappyTimes
    private ScheduledThreadPoolExecutor tPool;
    private ScheduledFuture<?> future;
    private Integer happyTimeSoundId;
    public static Context happyContext;
    
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Set up the mediaPlayer for background music
        mp = MediaPlayer.create(this, R.raw.lunchtime_meditation_session);
        mp.setLooping(true);
        try {
            mp.setOnPreparedListener(new OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer arg0) {
                    Log.v("wawco", "Started playback");
                    arg0.start();                
                }
            });
        }
        catch(Exception e) {
            Log.e("wawco", e.getMessage());
        }
        
        // Create a SoundPool and rig up each button to a particular sound
        
        soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        mapSoundToButtons = new HashMap<Integer, Integer>();
        buttonClicks = new HashMap<Integer, Boolean>();
        
        int maxVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
        
        // Loads the sounds and saves the sound id #'s in a map (for buttons)
        mapSoundToButtons.put(R.id.bottomLeftButton, soundPool.load(this, R.raw.button1sound, 1));
        mapSoundToButtons.put(R.id.bottomRightButton, soundPool.load(this, R.raw.button2sound, 1));
        mapSoundToButtons.put(R.id.topLeftButton, soundPool.load(this, R.raw.button3sound, 1));
        mapSoundToButtons.put(R.id.topRightButton, soundPool.load(this, R.raw.button4sound, 1));
        mapSoundToButtons.put(R.id.middleButton, soundPool.load(this, R.raw.button5sound, 1));
        
        happyTimeSoundId = soundPool.load(this, R.raw.happytimes, 1);
        
        // Button clicks is used to keep track of whether a button has been hit
        resetClicks();
        
        
        // Grab the keys of buttonClicks (mapSoundToButtons would work, too) and
        // iterate through each button to add a listener
        for(Integer id : buttonClicks.keySet()) {
            ImageButton button = (ImageButton) this.findViewById(id);
            
            button.setOnClickListener(this);
        }
        
        // Finally, initialize the thread pool to activate the happy time later,
        // as well as another thread
        tPool =  new ScheduledThreadPoolExecutor(1);
    }
    

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.release();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mp.pause();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mp.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
   }
    
    
    @Override
    public void onClick(View v) {
        int thisID = v.getId();
        @SuppressWarnings("unused")
        int soundPoolReturn = 0;
        int soundID = mapSoundToButtons.get(thisID);
        // play the associated sound
        soundPoolReturn = soundPool.play(soundID, 1, 1, 1, 0, 1);
        
        // Start animating if there isn't an animation already going
        Drawable bg = v.getBackground();
        if(bg instanceof AnimationDrawable) {
            ((AnimationDrawable) bg).start();
        }
        
        // call Main Activity's "wasClicked"                    
        wasClicked(thisID);
    }
    
   public void wasClicked(int buttonID) {
       buttonClicks.put(buttonID, true);
       if(!buttonClicks.containsValue(false)) {
           allWereClicked();
       }
   }
   
   public void resetClicks() {
       buttonClicks.put(R.id.bottomLeftButton, false);
       buttonClicks.put(R.id.bottomRightButton, false);
       buttonClicks.put(R.id.topLeftButton, false);
       buttonClicks.put(R.id.topRightButton, false);
       buttonClicks.put(R.id.middleButton, false);
   }
   
   /**
    * The magic happens here.  All of the buttons have been clicked, so now
    * we introduce a random period of time before the child activity is spawned.
    */
   private void allWereClicked() {
       
       // Queue up the happytimes if it isn't already present
       try {
           if(future == null || future.isDone()) {
               happyContext = this;
               future = tPool.schedule(new HappyTimesRunnable(this.happyTimeSoundId), 5L, TimeUnit.SECONDS);
           }
       } catch (Exception e) {
           Log.e("MainActivity", e.getMessage());
       }
       
       // Run the background animation
       View view = this.findViewById(R.id.mainBG);
       Drawable bg = view.getBackground();
       if(bg instanceof AnimationDrawable) {
           ((AnimationDrawable) bg).start();
       }
   }
}

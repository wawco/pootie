package com.wawco.pootie;

import android.content.Intent;

public class HappyTimesRunnable implements Runnable {
    
    private Integer sid;  // The SoundPool ID to pass to happyTimes activity
    
    public HappyTimesRunnable(Integer soundID) {
        super();
        sid = soundID;
    }
    
    @Override
    public void run() {
        Intent intent = new Intent(MainActivity.happyContext, HappyTimes.class);
        intent.putExtra("com.wawco.pootie.happyTimeSoundId", sid);
        MainActivity.happyContext.startActivity(intent);
    }

}

package com.wawco.pootie;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

public class HappyTimes extends Activity {
    private Integer soundID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_times);
        
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        ((MainActivity) MainActivity.happyContext).resetClicks();
        soundID = (Integer) extras.get("com.wawco.pootie.happyTimeSoundId");
        if(soundID != null && MainActivity.soundPool != null) {
            MainActivity.soundPool.play(soundID, 1, 1, 1, 0, 1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_happy_times, menu);
        return true;
    }
}
